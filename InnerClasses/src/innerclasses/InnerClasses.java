/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innerclasses;

/**
 *
 * @author david
 */
public class InnerClasses {

    private int tamano = 15;
    //Se crea un array para trabajar con el 
    private int [] arrayEnteros = new int [tamano];
    
    public InnerClasses (){
        //Se llena el array
        for (int i = 0; i < tamano; i++) {
            arrayEnteros[i] = i;
        }
    }
    
    public void imprimePar(){
        IteradorParInner it = this.new IteradorParInner();
        
        while (it.tieneSiguiente()) {
            System.out.println(it.obtenerSiguiente() + " ");
            //Imprime el valor del siguiente
        }
    }
    
    public class IteradorParInner {
        //Este es el iterador Inner donde se declaran los dos metodos donde se 
        //pueden acceder
        private int siguiente = 0;
        
        public boolean tieneSiguiente(){
            return (siguiente <= tamano - 1);
        }
        
        public int obtenerSiguiente(){
            int valor = arrayEnteros[siguiente];
            siguiente += 2;
            return valor;
                                       
        }
        
    }
    
    public static void main(String[] args) {
        InnerClasses ic = new InnerClasses();
        
        ic.imprimePar();
    
    }
    
}
